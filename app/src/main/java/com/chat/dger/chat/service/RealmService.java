package com.chat.dger.chat.service;

import android.app.Service;

import com.chat.dger.chat.bean.User;

import io.realm.Realm;

/**
 * Created by dgerasimov on 08.11.2017.
 */

public class  RealmService {

    public void saveUser(User user){
        Realm realm = Realm.getDefaultInstance();
        realm.copyToRealmOrUpdate(user);
    }
}
