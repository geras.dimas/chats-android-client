package com.chat.dger.chat.view;

import com.arellomobile.mvp.MvpView;
import com.chat.dger.chat.bean.Message;

import java.util.List;

/**
 * Created by dgerasimov on 09.11.2017.
 */

public interface ChatView extends MvpView {

    void setMessages(List<Message> messages);
}
