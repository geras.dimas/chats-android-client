package com.chat.dger.chat.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.chat.dger.chat.App;
import com.chat.dger.chat.R;
import com.chat.dger.chat.presenter.PhoneLoginPresenter;
import com.chat.dger.chat.view.PhoneLoginView;

public class PhoneLoginActivity extends MvpAppCompatActivity  implements PhoneLoginView{

    @InjectPresenter(type = PresenterType.GLOBAL)
    PhoneLoginPresenter phoneLoginPresenter;
    private EditText mPhoneNumberField;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);
        mPhoneNumberField = findViewById(R.id.phone_login);
        App app = App.get();
        if(app.getToken()!=null){
            succesVerify();
        }
    }

    @Override
    public void succesVerify() {
        Intent main = new Intent(this, ChatListActivity.class);
        startActivity(main);
        finish();
    }
    @Override
    public void setError(Integer idView, String errorMsg) {
        switch (idView) {
            case R.id.phone_login:
                mPhoneNumberField.setError(errorMsg);
                break;
        }

    }
    public void clickPhoneButton(View view) {

        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return;
        }
        phoneLoginPresenter.sendNumber(phoneNumber);
    }
}
