package com.chat.dger.chat.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

/**
 * Created by egf on 11.09.17.
 */

public class ImageView extends SimpleDraweeView {

    public interface EventListener {
        void failed();
    }

    public synchronized boolean isFailed() {
        return failed;
    }

    private volatile boolean failed;

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    private EventListener eventListener;

    private final BaseControllerListener<ImageInfo> baseControllerListener = new BaseControllerListener<ImageInfo>() {
        @Override
        public void onFailure(String id, Throwable throwable) {
            synchronized (ImageView.this){
                failed = true;
            }
            if(eventListener!=null)eventListener.failed();
        }
    };

    public ImageView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
        init();
    }

    public ImageView(Context context) {
        super(context);
        init();
    }

    public ImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                //.setImageRequest(request)
                .setControllerListener(baseControllerListener)
                .build();
        setController(controller);
    }
}
