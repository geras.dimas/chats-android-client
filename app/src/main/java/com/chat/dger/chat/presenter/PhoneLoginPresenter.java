package com.chat.dger.chat.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.chat.dger.chat.App;
import com.chat.dger.chat.R;
import com.chat.dger.chat.model.AuthModel;
import com.chat.dger.chat.model.dto.AuthorizationServerAnswerDTO;
import com.chat.dger.chat.view.PhoneLoginView;

/**
 * Created by dgerasimov on 01.11.2017.
 */

@InjectViewState
public class PhoneLoginPresenter extends MvpPresenter<PhoneLoginView> {

    private AuthModel authModel;

    public PhoneLoginPresenter(){
        authModel = new AuthModel();
    }

    public void sendNumber(String number) {
        authModel.getKeys(number).subscribe(universalGeoEntityList -> setJwt(universalGeoEntityList), error -> getViewState().setError(R.id.phone_login,error.getMessage()));
        String s= "sdd";
    }
    private void setJwt(AuthorizationServerAnswerDTO jwt){
        App.get().setToken(jwt);
        getViewState().succesVerify();
    }

}
