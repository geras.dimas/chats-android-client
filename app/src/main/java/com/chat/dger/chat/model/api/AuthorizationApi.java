package com.chat.dger.chat.model.api;

import com.chat.dger.chat.model.dto.AuthorizationServerAnswerDTO;
import com.chat.dger.chat.model.dto.FirebaseTokenDTO;
import com.chat.dger.chat.model.dto.PhoneNumberDTO;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by dgerasimov on 24.10.2017.
 */

public interface AuthorizationApi {
    /*@POST("/login")
    Observable<AuthorizationServerAnswerDTO> getKeys(@Body FirebaseTokenDTO firebaseTokenDTO);*/
    @POST("/login")
    Observable<AuthorizationServerAnswerDTO> getKeys(@Body PhoneNumberDTO number);
}
