package com.chat.dger.chat.view;

import com.arellomobile.mvp.MvpView;

/**
 * Created by dgerasimov on 14.11.2017.
 */

public interface AddChatView extends MvpView{
    void setError(Integer idView, String errorMsg);
    void cancelActivity();
}
