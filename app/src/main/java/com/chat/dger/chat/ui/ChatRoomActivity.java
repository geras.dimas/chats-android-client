package com.chat.dger.chat.ui;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.chat.dger.chat.App;
import com.chat.dger.chat.R;
import com.chat.dger.chat.bean.Chat;
import com.chat.dger.chat.bean.Message;
import com.chat.dger.chat.presenter.ChatPresenter;
import com.chat.dger.chat.ui.adapters.ChatListAdapter;
import com.chat.dger.chat.ui.adapters.MessageAdapter;
import com.chat.dger.chat.view.ChatView;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ChatRoomActivity extends MvpAppCompatActivity implements ChatView {

    @InjectPresenter
    ChatPresenter chatPresenter;

    ListView messageListView;
    EditText message;
    ImageButton sendMessage;
    ImageButton sendFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        String chatId = "";
        chatId = getIntent().getStringExtra("chatId");
        try {
            chatPresenter.setChat(chatId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Chat chat = chatPresenter.getChat();
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_chat);
        myToolbar.setTitle(chat.getChatName());
        setSupportActionBar(myToolbar);
        messageListView = (ListView) findViewById(R.id.message_list);
        chatPresenter.getMessageList();
        sendMessage = (ImageButton) findViewById(R.id.sendMessageButton);
        sendFile = (ImageButton) findViewById(R.id.addFileButton);
        message = (EditText) findViewById(R.id.messageEditText);
    }

    public void setMessages(List<Message> messages) {
        MessageAdapter messageAdapter = new MessageAdapter(this, R.layout.chat_list_item, messages);
        messageListView.setAdapter(messageAdapter);
    }

    public void clickSendMessageButton(View view) {

        String messageText = message.getText().toString();
        if (TextUtils.isEmpty(messageText)) {

            return;
        }
        chatPresenter.sendMessage(messageText);
    }

    public void clickAddFile(View view) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        final int SELECT_PHOTO = 1234;
        photoPickerIntent.setType("*/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 1234:
                if (resultCode == RESULT_OK) {

                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        InputStream fileStream = getContentResolver().openInputStream(selectedImage);
                        String displayName = "";
                        long size = 0;
                        Cursor cursor = getContentResolver()
                                .query(selectedImage, null, null, null, null, null);

                        try {
                            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
                            // "if there's anything to look at, look at it" conditionals.
                            if (cursor != null && cursor.moveToFirst()) {
                                displayName = cursor.getString(
                                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                size = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
                            }
                        } finally {
                            cursor.close();
                        }
                        byte[] bytes = readBytes(fileStream);

                        //8.96
                        System.out.println("good");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    private byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }
}
