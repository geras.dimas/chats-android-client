package com.chat.dger.chat.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.chat.dger.chat.R;
import com.chat.dger.chat.bean.Chat;

import java.util.List;

/**
 * Created by dgerasimov on 26.10.2017.
 */

public class ChatListAdapter extends ArrayAdapter <Chat> {
    private LayoutInflater inflater;
    private int layout;
    private List<Chat> chats;

    public ChatListAdapter(Context context, int layout, List<Chat> chats) {
        super(context, layout, chats);
        this.inflater = LayoutInflater.from(context);
        this.layout = layout;
        this.chats = chats;
    }
    public View getView(int position, View convertView, ViewGroup parent){
        View view=inflater.inflate(this.layout,parent,false);

        TextView header=(TextView) view.findViewById(R.id.chatName);
        TextView lastMessage = (TextView) view.findViewById(R.id.lastMessage);

        Chat chat = chats.get(position);
        header.setText(chat.getChatName());

        if(chat.getLastMessage()!=null) {
            lastMessage.setText(chat.getLastMessage().getMessage());
        }
        return view;
    }
}
