package com.chat.dger.chat.bean;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by dgerasimov on 08.11.2017.
 */

public class User extends RealmObject {
    @PrimaryKey
    private String _id;

    private String name;

    @Required
    private String tel;

    private boolean isLocalUser;

    private FileRealm avatar;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public boolean isLocalUser() {
        return isLocalUser;
    }

    public void setLocalUser(boolean localUser) {
        isLocalUser = localUser;
    }

    public FileRealm getAvatar() {
        return avatar;
    }

    public void setAvatar(FileRealm avatar) {
        this.avatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!_id.equals(user._id)) return false;
        return tel.equals(user.tel);
    }

}
