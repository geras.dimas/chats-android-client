package com.chat.dger.chat.ui.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.chat.dger.chat.App;
import com.chat.dger.chat.R;
import com.chat.dger.chat.bean.Message;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dgerasimov on 10.11.2017.
 */

public class MessageAdapter extends ArrayAdapter<Message> {
    private LayoutInflater inflater;
    private int layout;
    private List<Message> messages;
    private String localUserId;

    public MessageAdapter(Context context, int layout, List<Message> messages) {
        super(context, layout, messages);
        this.inflater = LayoutInflater.from(context);
        this.layout = layout;
        this.messages = messages;
        this.localUserId = App.get().getLocalUserId();

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(this.layout, parent, false);

        Message message = messages.get(position);
        view = inflater.inflate(message.getAuthor().get_id().equals(localUserId) ? R.layout.chat_message_bubble_right : R.layout.chat_message_bubble_left, null);
        TextView nickTextView = view.findViewById(R.id.nick);
        nickTextView.setText(message.getAuthor().getTel()!=null?message.getAuthor().getTel():"anonimus");
        TextView msg = view.findViewById(R.id.message_text);
        CircleImageView avatar = view.findViewById(R.id.avatar_view);
        if(message.getAuthor().getAvatar()!=null){
            File imgFile = new  File(App.get().getFilesDir() + message.getAuthor().getAvatar().getFileName());
            String mimeType =  MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(App.get().getFilesDir() + message.getAuthor().getAvatar().getFileName()));
            System.out.println("mimetype filee - "+mimeType);
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                avatar.setImageBitmap(myBitmap);
            }
        }
        msg.setText(message.getMessage());
        return view;
    }
}

