package com.chat.dger.chat;

import android.content.SharedPreferences;
import android.net.Uri;


import com.chat.dger.chat.bean.FileRealm;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import io.realm.Realm;

/**
 * Created by dgerasimov on 17.11.2017.
 */

public class FileService {

    private static final String PREFS_CONFIG_FILE = "Config";
    private static final String PREFS_CONFIG_STORAGE_AVATAR_CONTAINER = "AvatarStorageContainer";
    private static final String PREFS_CONFIG_STORAGE_CONTAINER = "StorageContainer";
    private static final String PREFS_CONFIG_STORAGE = "Storage";
    private static final String PREFS_CONFIG_MAX_SIZE_AVATAR = "maxSizeAvatar";
    private static final String PREFS_CONFIG_MAX_SIZE_FILE = "maxSizeFile";
    private static final String PREFS_CONFIG_POINT_GET_TIME_INTERVAL = "pointGetTimeInterval";
    private static final String PREFS_CONFIG_POINT_GET_INTERVAL = "pointGetInterval";
    private static FileService instance;
    private ThinDownloadManager downloadManager;
    private SharedPreferences configSettings;
    SharedPreferences.Editor prefEditor;

    public FileService() {
        instance = this;
        downloadManager = new ThinDownloadManager();
        configSettings = App.get().getConfigSettings();
    }

    public static FileService get() {
        return instance;
    }

    public void addDownloadRequest(FileRealm file) {

        File fileLocal = new File(App.get().getFilesDir() + file.getFileName());
        if(!fileLocal.exists()) {
            Uri downloadUri = Uri.parse(configSettings.getString(PREFS_CONFIG_STORAGE, "") + "/" + configSettings.getString(PREFS_CONFIG_STORAGE_AVATAR_CONTAINER, "")+"/"+file.getId());
            Uri destinationUri = Uri.parse(App.get().getFilesDir() + file.getFileName());
            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                    .setRetryPolicy(new DefaultRetryPolicy())
                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.NORMAL)
                    .setDownloadContext(file.getId())//Optional
                    .setStatusListener(new DownloadStatusListenerV1() {
                        @Override
                        public void onDownloadComplete(DownloadRequest dr) {
                            System.out.println("good");
                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            FileRealm  fileRealm=realm.where(FileRealm.class).equalTo("id",(String)dr.getDownloadContext()).findFirst();
                            fileRealm.setUpdateDate(new Date());
                            realm.copyToRealmOrUpdate(fileRealm);
                            realm.commitTransaction();
                            realm.close();

                        }

                        @Override
                        public void onDownloadFailed(DownloadRequest dr, int errorCode, String errorMessage) {
                            System.out.println(errorMessage);
                        }

                        @Override
                        public void onProgress(DownloadRequest dr, long totalBytes, long downlaodedBytes, int progress) {
                            System.out.println(progress);

                        }
                    });

                downloadManager.add(downloadRequest);

        }
    }

    public void deleteFile(String fileName){
        File imgFile = new  File(App.get().getFilesDir() + fileName);
        imgFile.delete();
    }

    private byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }
}
