package com.chat.dger.chat.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.chat.dger.chat.App;
import com.chat.dger.chat.bean.User;
import com.chat.dger.chat.view.NavView;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by dgerasimov on 17.11.2017.
 */
@InjectViewState
public class NavPresenter extends MvpPresenter<NavView> {
    private RealmResults<User> realmResults;
    private OrderedRealmCollectionChangeListener<RealmResults<User>> changeListener;
    Realm realm;

    public NavPresenter() {
        realm = Realm.getDefaultInstance();
        realmResults = realm.where(User.class).equalTo("_id", App.get().getLocalUserId()).findAll();
        changeListener = (collection, changeSet) -> {
            // `null`  means the async query returns the first time.
            if (changeSet == null) {
                return;
            }
            // For deletions, the adapter has to be notified in reverse order.
            OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
            for (int i = deletions.length - 1; i >= 0; i--) {
                OrderedCollectionChangeSet.Range range = deletions[i];
            }
            OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
            for (OrderedCollectionChangeSet.Range range : insertions) {
                setCurrentUser();
            }

            OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
            for (OrderedCollectionChangeSet.Range range : modifications) {
                setCurrentUser();
            }
        };
        realmResults.removeAllChangeListeners();
        realmResults.addChangeListener(changeListener);
    }

    public void setCurrentUser() {
        Realm realm = Realm.getDefaultInstance();
        User currentUser = realm.where(User.class).equalTo("_id", App.get().getLocalUserId()).findFirst();
        if (currentUser != null) {
            getViewState().setCurrentUser(currentUser);
        }
        realm.close();
    }

}
