package com.chat.dger.chat.ui;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.MimeTypeMap;

import com.chat.dger.chat.App;
import com.chat.dger.chat.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.UUID;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                photoPickerIntent.setType("image/*");
                final int SELECT_PHOTO = 1234;
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 1234:
                if (resultCode == RESULT_OK) {
                    String uuid = UUID.randomUUID().toString();
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {

                        InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                        String displayName = "";
                        long size = 0;
                        String type;

                        Cursor cursor = getContentResolver()
                                .query(selectedImage, null, null, null, null, null);

                        try {
                            if (cursor != null && cursor.moveToFirst()) {
                                displayName = cursor.getString(
                                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                size = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
                            }
                        } finally {
                            cursor.close();
                        }
                        String extension = MimeTypeMap.getFileExtensionFromUrl(displayName);
                        if (extension != null) {
                            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                        }
                        JSONObject json = new JSONObject();
                        JSONObject data = new JSONObject();
                        try {
                            json.put("id", 123);
                            data.put("name", displayName);
                            data.put("uuid", uuid);
                            data.put("size", size);
                            json.put("data", data);

                            App.get().getSocket().emit("uploadAvatar", json);

                            // this is storage overwritten on each iteration with bytes
                            int bufferSize = 1024 * 128;
                            byte[] buffer = new byte[size < bufferSize ? (int) size : bufferSize];

                            // we need to know how may bytes were read to write them to the byteBuffer
                            int len = 0;
                            long len2 = 0;
                            while ((len = imageStream.read(buffer)) != -1) {
                                len2 += buffer.length;
                                byte[] finalBuffer;
                                if(len2<bufferSize){
                                    finalBuffer = Arrays.copyOf(buffer, len);
                                }
                                else
                                {
                                    finalBuffer = buffer;
                                }
                                JSONObject jsonData = new JSONObject();
                                JSONObject dataData = new JSONObject();
                                jsonData.put("id", 123);
                                dataData.put("chunk", finalBuffer);
                                jsonData.put("data", dataData);
                                App.get().getSocket().emit("uploadAvatar-data", jsonData);
                            }
                            JSONObject jsonData = new JSONObject();
                            JSONObject dataData = new JSONObject();
                            jsonData.put("id", 123);
                            App.get().getSocket().emit("uploadAvatar-end", jsonData);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("good");
                        //8.96
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }

    }

    private byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }


}
