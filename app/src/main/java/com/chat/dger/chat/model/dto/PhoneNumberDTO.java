package com.chat.dger.chat.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dgerasimov on 01.11.2017.
 */

public class PhoneNumberDTO {
    @SerializedName("number")
    @Expose
    private String number;

    public PhoneNumberDTO() {
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
