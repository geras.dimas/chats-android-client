package com.chat.dger.chat.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.chat.dger.chat.R;
import com.chat.dger.chat.presenter.AddChatPresenter;
import com.chat.dger.chat.view.AddChatView;

public class AddChatActivity extends MvpAppCompatActivity implements AddChatView{

    @InjectPresenter
    AddChatPresenter addChatPresenter;

    TextView chatHeader;
    TextView lastMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_chat);
        chatHeader = (TextView) findViewById(R.id.chat_header_add);
    }

    @Override
    public void setError(Integer idView, String errorMsg) {
        switch (idView) {
            case R.id.chat_header_add:
                chatHeader.setError(errorMsg);
                break;
        }

    }

    public void clickCancelButton(View view) {
        cancelActivity();
    }

    public void clickCreateChatButton(View view) {
        String header = chatHeader.getText().toString();
        addChatPresenter.createChat(header);
        cancelActivity();
    }


    @Override
    public void cancelActivity() {
        Intent main = new Intent(this, ChatListActivity.class);
        startActivity(main);
        finish();
    }

}
