package com.chat.dger.chat.view;

import com.arellomobile.mvp.MvpView;
import com.chat.dger.chat.bean.Chat;

import java.util.List;

/**
 * Created by dgerasimov on 01.11.2017.
 */

public interface ChatListView extends MvpView {
    void setChats(List<Chat> chats);
}
