package com.chat.dger.chat.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.chat.dger.chat.view.SettingsView;

/**
 * Created by dgerasimov on 22.11.2017.
 */

@InjectViewState
public class SettingsPresenter extends MvpPresenter<SettingsView> {
}
