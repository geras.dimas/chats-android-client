package com.chat.dger.chat.presenter;

import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.chat.dger.chat.App;
import com.chat.dger.chat.bean.Chat;
import com.chat.dger.chat.bean.FileRealm;
import com.chat.dger.chat.bean.Message;
import com.chat.dger.chat.bean.User;
import com.chat.dger.chat.enums.MessageState;
import com.chat.dger.chat.enums.MessageType;
import com.chat.dger.chat.view.ChatView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;
import io.socket.client.Socket;

/**
 * Created by dgerasimov on 09.11.2017.
 */
@InjectViewState
public class ChatPresenter extends MvpPresenter<ChatView> {
    private Socket mSocket;
    private  RealmResults<Message> realmResults;
    private Chat chat;
    private  OrderedRealmCollectionChangeListener<RealmResults<Message>> changeListener;
    Realm realm;

    public ChatPresenter()
    {
        realm = Realm.getDefaultInstance();
        mSocket = App.get().getSocket();
        changeListener = (collection, changeSet) -> {
            // `null`  means the async query returns the first time.
            if (changeSet == null) {
                return;
            }
            // For deletions, the adapter has to be notified in reverse order.
            OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
            for (int i = deletions.length - 1; i >= 0; i--) {
                OrderedCollectionChangeSet.Range range = deletions[i];
            }
            OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
            for (OrderedCollectionChangeSet.Range range : insertions) {
                getMessageList();
            }

            OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
            for (OrderedCollectionChangeSet.Range range : modifications) {
                System.out.println("test");
            }
        };

    }

    public void setChat(String chatId) throws JSONException {
        realmResults = realm.where(Message.class).equalTo("chatId",chatId).findAll();
        realmResults.removeAllChangeListeners();
        realmResults.addChangeListener(changeListener);
        chat = realm.where(Chat.class).equalTo("id", chatId).findFirst();
        JSONObject json = new JSONObject();
        json.put("id", 123);
        JSONObject query = new JSONObject();
        query.put("chatId", chatId);
        json.putOpt("data", query);
        mSocket.emit("getChatMessages", json);

    }

    public void getMessageList(){

        List<Message>  messages = realmResults.sort("createDate");

        getViewState().setMessages(messages);
    }

    public Chat getChat() {
        return chat;
    }

    public void sendMessage(String message){
        realm.beginTransaction();
        Message messageRealm = new Message();
        messageRealm.setAuthor(realm.where(User.class).equalTo("_id", App.get().getLocalUserId()).findFirst());
        messageRealm.setState(MessageState.UNSENT.getValue());
        messageRealm.setId(UUID.randomUUID().toString());
        messageRealm.setChatId(chat.getId());
        messageRealm.setType(MessageType.MESSAGE.getValue());
        messageRealm.setCreateDate(new Date());
        messageRealm.setMessage(message);
        realm.copyToRealmOrUpdate(messageRealm);
        App.get().sendMessage(messageRealm);
        realm.commitTransaction();
    }

    public void sendFile(InputStream fileStream, String fileName, int fileSize, String type){
        realm.beginTransaction();
        FileRealm file = new FileRealm();
        file.setId(UUID.randomUUID().toString());
        file.setSize(fileSize);
        file.setFileName(fileName);
        file.setType(type);
        file=realm.copyToRealmOrUpdate(file);
        Message message = new Message();
        message.setId(UUID.randomUUID().toString());
        message.setFile(file);
        message.setAuthor(realm.where(User.class).equalTo("_id", App.get().getLocalUserId()).findFirst());
        message.setState(MessageState.UNSENT.getValue());
        message.setChatId(chat.getId());
        message.setType(MessageType.FILE.getValue());
        message.setCreateDate(new Date());
        message.setMessage("");
        realm.copyToRealmOrUpdate(message);
        App.get().sendMessage(message);
        realm.commitTransaction();

    }

    public void closeRealm(){
        realm.close();
    }
}
