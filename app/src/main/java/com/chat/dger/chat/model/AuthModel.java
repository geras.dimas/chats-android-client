package com.chat.dger.chat.model;




import com.chat.dger.chat.model.api.AuthorizationApi;
import com.chat.dger.chat.model.dto.AuthorizationServerAnswerDTO;
import com.chat.dger.chat.model.dto.FirebaseTokenDTO;
import com.chat.dger.chat.model.dto.PhoneNumberDTO;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dgerasimov on 24.10.2017.
 */

public class AuthModel {
    @Inject
    private AuthorizationApi authorizationApi;
    private Retrofit retrofit;
    public AuthModel(){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:3000/") //Базовая часть адреса
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        authorizationApi = retrofit.create(AuthorizationApi.class);
    }

    /*public Observable<AuthorizationServerAnswerDTO> getKeys(String firebaseToken){
        FirebaseTokenDTO firebaseTokenDTO=new FirebaseTokenDTO();
        firebaseTokenDTO.setFirebaseToken(firebaseToken);
        return authorizationApi.getKeys(firebaseTokenDTO).subscribeOn(Schedulers.newThread());

    }*/
    public Observable<AuthorizationServerAnswerDTO> getKeys(String number){
        PhoneNumberDTO phoneNumberDTO = new PhoneNumberDTO();
        phoneNumberDTO.setNumber(number);
        return authorizationApi.getKeys(phoneNumberDTO).subscribeOn(Schedulers.newThread());

    }
}
