package com.chat.dger.chat.enums;

/**
 * Created by dgerasimov on 01.12.2017.
 */

public enum MessageState {
    SEND(1), UNSENT(0);
    private final int value;
    MessageState(int value) {
        this.value=value;
    }
    public int getValue(){
        return  this.value;
    }
}
