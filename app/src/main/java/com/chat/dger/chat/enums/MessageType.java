package com.chat.dger.chat.enums;

/**
 * Created by dgerasimov on 01.12.2017.
 */

public enum MessageType {
    FILE(2), MESSAGE(1);
    private final int value;
    MessageType(int value) {
        this.value=value;
    }
    public int getValue(){
        return  this.value;
    }
}
