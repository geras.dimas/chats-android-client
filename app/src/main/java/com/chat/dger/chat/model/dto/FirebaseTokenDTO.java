package com.chat.dger.chat.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dgerasimov on 24.10.2017.
 */

public class FirebaseTokenDTO {
    @SerializedName("firebaseToken")
    @Expose
    private String firebaseToken;

    public FirebaseTokenDTO() {
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }
}
