package com.chat.dger.chat.view;

import com.arellomobile.mvp.MvpView;
import com.chat.dger.chat.bean.User;

/**
 * Created by dgerasimov on 17.11.2017.
 */

public interface NavView extends MvpView{
    void setCurrentUser(User currentUser);
}
