package com.chat.dger.chat.view;

import com.arellomobile.mvp.MvpView;

/**
 * Created by dgerasimov on 01.11.2017.
 */

public interface PhoneLoginView extends MvpView {
    void setError(Integer idView, String errorMsg);
    void succesVerify();
}

