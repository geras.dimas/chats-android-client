package com.chat.dger.chat.ui;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.chat.dger.chat.App;
import com.chat.dger.chat.R;
import com.chat.dger.chat.bean.Chat;
import com.chat.dger.chat.presenter.ChatListPresenter;
import com.chat.dger.chat.ui.adapters.ChatListAdapter;
import com.chat.dger.chat.view.ChatListView;

import java.util.List;

public class ChatListActivity extends NavigationActivity implements ChatListView {

    @InjectPresenter
    ChatListPresenter chatListPresenter;

    ListView chatListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        onCreateNavigation();
        chatListView = (ListView) findViewById(R.id.chat_list);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent main = new Intent(App.get(), AddChatActivity.class);
                startActivity(main);*/
                Intent intent = new Intent(App.get(),AddChatActivity.class);

                startActivityForResult(intent, 12);
            }
        });
        chatListPresenter.getChatList();

    }
    @Override
    protected void onStart() {
        super.onStart();
        onCreateNavigation();
    }
    @Override
    public void setChats(List<Chat> chatList) {
        ChatListAdapter chatListAdapter = new ChatListAdapter(this, R.layout.chat_list_item, chatList);
        chatListView.setAdapter(chatListAdapter);
        chatListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                Intent intent = new Intent(App.get(), ChatRoomActivity.class);
                Chat chat = (Chat)chatListView.getItemAtPosition(position);
                intent.putExtra("chatId", chat.getId());
                startActivity(intent);
            }
        });
    }
}
