package com.chat.dger.chat;

import android.app.Application;

import com.chat.dger.chat.bean.Chat;
import com.chat.dger.chat.bean.FileRealm;
import com.chat.dger.chat.bean.Message;
import com.chat.dger.chat.bean.User;
import com.chat.dger.chat.enums.MessageState;
import com.chat.dger.chat.model.dto.AuthorizationServerAnswerDTO;
import com.facebook.drawee.backends.pipeline.Fresco;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;

import io.realm.Realm;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by dgerasimov on 01.11.2017.
 */

public class App extends Application {
    private AuthorizationServerAnswerDTO token;

    private Socket mSocket;

    private SharedPreferences profileSettings;
    private SharedPreferences configSettings;
    private static final String PREFS_PROFILE_FILE = "Profile";
    private static final String PREFS_CONFIG_FILE = "Config";
    private static final String PREFS_CONFIG_STORAGE_AVATAR_CONTAINER = "AvatarStorageContainer";
    private static final String PREFS_CONFIG_STORAGE_CONTAINER = "StorageContainer";
    private static final String PREFS_CONFIG_STORAGE = "Storage";
    private static final String PREFS_CONFIG_MAX_SIZE_AVATAR = "maxSizeAvatar";
    private static final String PREFS_CONFIG_MAX_SIZE_FILE = "maxSizeFile";
    private static final String PREFS_CONFIG_POINT_GET_TIME_INTERVAL = "pointGetTimeInterval";
    private static final String PREFS_CONFIG_POINT_GET_INTERVAL = "pointGetInterval";
    private static final String PREF_PROFILE_ID = "Id";
    private SharedPreferences.Editor prefEditor;
    private FileService fileService;
    private static App instance;


    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        instance = this;
        token = null;
        mSocket = null;
        profileSettings = getSharedPreferences(PREFS_PROFILE_FILE, MODE_PRIVATE);
        configSettings = getSharedPreferences(PREFS_CONFIG_FILE, MODE_PRIVATE);
        Fresco.initialize(this);
        fileService = new FileService();
    }

    public static App get() {
        return instance;
    }


    public AuthorizationServerAnswerDTO getToken() {
        return token;
    }

    public void setToken(AuthorizationServerAnswerDTO token) {
        this.token = token;
        startSocket();
    }

    //TODO перенести сокеты в отдельный класс
    private void startSocket() {

        if (this.token != null) {
            JSONObject json = new JSONObject();
            IO.Options opt = new IO.Options();
            opt.forceNew = true;
            opt.reconnection = true;
            opt.query = "token=" + token.getToken();
            String url = "http://10.0.2.2:3000/";
            try {
                this.mSocket = IO.socket(url, opt);
                json.putOpt("token", token.getToken());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mSocket.emit("authenticate", json);
            mSocket.on("authenticated", new Emitter.Listener() {

                @Override
                public void call(Object... args) {

                }

            });

            mSocket.on("getUser", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    JSONObject data = (JSONObject) args[0];
                    JSONObject user = new JSONObject();
                    try {
                        user = data.getJSONObject("data");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        User userRealm = realm.where(User.class).equalTo("_id", user.getString("_id")).findFirst();
                        if (userRealm == null) {
                            userRealm = new User();
                            userRealm.set_id(user.getString("_id"));
                        }
                        userRealm.setTel(user.getString("tel"));
                        prefEditor = profileSettings.edit();
                        prefEditor.putString(PREF_PROFILE_ID, user.getString("_id"));
                        prefEditor.apply();
                        userRealm.setLocalUser(true);
                        if (user.has("avatar")) {
                            JSONObject avatar = user.getJSONObject("avatar");
                            FileRealm avatarRealm = realm.where(FileRealm.class).equalTo("id", avatar.getString("_id")).findFirst();
                            if (avatarRealm == null) {
                                avatarRealm = new FileRealm();
                                avatarRealm.setId(avatar.getString("_id"));
                                avatarRealm.setFileName(avatar.getString("fileName"));
                                avatarRealm.setType(avatar.getString("type"));
                                avatarRealm.setSize(avatar.getInt("size"));
                                avatarRealm = realm.copyToRealmOrUpdate(avatarRealm);
                            }
                            FileRealm oldAvatar = userRealm.getAvatar();
                            if(oldAvatar != null && !oldAvatar.getId().equals(avatar.getString("_id"))){
                                userRealm.setAvatar(null);
                                JSONObject json = new JSONObject();
                                json.put("id",123);
                                JSONObject delFile = new JSONObject();
                                delFile.put("fileId",oldAvatar.getId());
                                delFile.put("isAvatar",true);
                                json.put("data",delFile);
                                mSocket.emit("deleteFile",json);
                                fileService.deleteFile(oldAvatar.getFileName());
                                oldAvatar.deleteFromRealm();
                            }
                            userRealm.setAvatar(avatarRealm);
                            fileService.addDownloadRequest(avatarRealm);
                        }
                        realm.copyToRealmOrUpdate(userRealm);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    realm.commitTransaction();
                    realm.close();
                }

            });

            mSocket.on("getUserById", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    JSONObject data = (JSONObject) args[0];
                    JSONObject user = new JSONObject();
                    try {
                        user = data.getJSONObject("data");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    User userRealm = new User();
                    try {
                        userRealm.set_id(user.getString("_id"));
                        userRealm.setTel(user.getString("tel"));
                        userRealm.setLocalUser(false);
                        if (user.has("avatar")) {
                            JSONObject avatar = user.getJSONObject("avatar");
                            FileRealm avatarRealm = realm.where(FileRealm.class).equalTo("id", avatar.getString("_id")).findFirst();
                            if (avatarRealm == null) {
                                avatarRealm = new FileRealm();
                                avatarRealm.setId(avatar.getString("_id"));
                                avatarRealm.setFileName(avatar.getString("fileName"));
                                avatarRealm.setType(avatar.getString("type"));
                                avatarRealm.setSize(avatar.getInt("size"));
                                avatarRealm = realm.copyToRealmOrUpdate(avatarRealm);
                            }
                            FileRealm oldAvatar = userRealm.getAvatar();
                            if(oldAvatar != null && !oldAvatar.getId().equals(avatar.getString("_id"))){
                                userRealm.setAvatar(null);
                                JSONObject json = new JSONObject();
                                json.put("id",123);
                                JSONObject delFile = new JSONObject();
                                delFile.put("fileId",oldAvatar.getId());
                                delFile.put("isAvatar",true);
                                json.put("data",delFile);
                                fileService.deleteFile(oldAvatar.getFileName());
                                oldAvatar.deleteFromRealm();
                            }
                                userRealm.setAvatar(avatarRealm);
                            fileService.addDownloadRequest(avatarRealm);
                        }
                        realm.copyToRealmOrUpdate(userRealm);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    realm.commitTransaction();
                    realm.close();
                }

            });
            mSocket.on("getChatMessages", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    Realm realm = Realm.getDefaultInstance();
                    JSONObject data = (JSONObject) args[0];
                    JSONArray messages = new JSONArray();
                    try {
                        messages = data.getJSONArray("data");
                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        for (int i = 0; i < messages.length(); i++) {
                            JSONObject message = messages.getJSONObject(i);
                            Message messageRealm = new Message();
                            messageRealm.setCreateDate(format.parse(message.getString("createdAt")));
                            messageRealm.setId(message.getString("_id"));
                            messageRealm.setMessage(message.getString("message"));
                            messageRealm.setChatId(message.getString("chatId"));
                            messageRealm.setState(MessageState.SEND.getValue());
                            JSONObject authorJson = message.getJSONObject("author");
                            User author = realm.where(User.class).equalTo("_id", authorJson.getString("_id")).findFirst();
                            if (author == null) {
                                author = new User();
                                author.set_id(authorJson.getString("_id"));
                                author.setTel(authorJson.getString("tel"));
                            }
                            messageRealm.setAuthor(author);
                            if(message.has("file")) {
                                JSONObject file = message.getJSONObject("file");
                                FileRealm fileRealm = realm.where(FileRealm.class).equalTo("id",file.getString("_id")).findFirst();
                                if(fileRealm == null){
                                    fileRealm = new FileRealm();
                                    fileRealm.setId(file.getString("_id"));
                                }
                                fileRealm.setFileName(file.getString("fileName"));
                                fileRealm.setType(file.getString("type"));
                                fileRealm.setSize(file.getInt("size"));
                                messageRealm.setFile(fileRealm);
                            }

                            realm.executeTransaction(new Realm.Transaction() {

                                @Override
                                public void execute(Realm realm) {
                                    realm.copyToRealmOrUpdate(messageRealm);
                                }
                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    realm.close();
                    System.out.println("====getMessagesChat===");
                }

            });
            mSocket.on("getUserChats", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    JSONObject data = (JSONObject) args[0];
                    JSONArray chats = new JSONArray();
                    try {
                        chats = data.getJSONArray("data");
                        for (int i = 0; i < chats.length(); i++) {
                            JSONObject chat = new JSONObject();
                            try {
                                chat = chats.getJSONObject(i);

                                Chat chatRealm = realm.where(Chat.class).equalTo("id", chat.getString("_id")).findFirst();
                                if (chatRealm == null) {
                                    chatRealm = new Chat();
                                    chatRealm.setId(chat.getString("_id"));
                                    chatRealm.setChatName(chat.getString("header"));
                                    chatRealm.setOwner(chat.getString("owner"));
                                }
                                Message lastMessageRealm = new Message();
                                if (chat.has("lastMessage")) {
                                    JSONObject lastMessage = chat.getJSONObject("lastMessage");

                                    lastMessageRealm.setChatId(lastMessage.getString("chatId"));
                                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                    lastMessageRealm.setId(lastMessage.getString("_id"));
                                    lastMessageRealm.setMessage(lastMessage.getString("message"));
                                    lastMessageRealm.setCreateDate(format.parse(lastMessage.getString("createdAt")));
                                }


                                JSONArray members = chat.getJSONArray("members");
                                for (int j = 0; j < members.length(); j++) {
                                    JSONObject member = members.getJSONObject(j);
                                    User user = realm.where(User.class).equalTo("_id", member.getString("_id")).findFirst();
                                    String tel = member.getString("tel");
                                    if (user == null) {
                                        user = new User();
                                        user.set_id(member.getString("_id"));
                                    }
                                    user.setTel(member.getString("tel"));
                                    user.setName("");
                                    user.setLocalUser(false);
                                    user = realm.copyToRealmOrUpdate(user);
                                    if (member.has("avatar")) {
                                        JSONObject avatar = member.getJSONObject("avatar");
                                        FileRealm avatarRealm = realm.where(FileRealm.class).equalTo("id", avatar.getString("_id")).findFirst();
                                        if (avatarRealm == null) {
                                            avatarRealm = new FileRealm();
                                            avatarRealm.setId(avatar.getString("_id"));
                                            avatarRealm.setFileName(avatar.getString("fileName"));
                                            avatarRealm.setType(avatar.getString("type"));
                                            avatarRealm.setSize(avatar.getInt("size"));
                                            avatarRealm = realm.copyToRealmOrUpdate(avatarRealm);
                                        }
                                        fileService.addDownloadRequest(avatarRealm);
                                        user.setAvatar(avatarRealm);
                                    }
                                    user = realm.copyToRealmOrUpdate(user);
                                    if (chat.has("lastMessage")) {
                                        JSONObject lastMessage = chat.getJSONObject("lastMessage");
                                        if (user.get_id().equals(lastMessage.getString("author"))) {
                                            lastMessageRealm.setAuthor(user);
                                            lastMessageRealm = realm.copyToRealmOrUpdate(lastMessageRealm);
                                            chatRealm.setLastMessage(lastMessageRealm);
                                        }
                                    }
                                    if (!chatRealm.getMembers().contains(user)) {
                                        chatRealm.addMember(user);
                                    }
                                }


                                realm.copyToRealmOrUpdate(chatRealm);


                                System.out.println("======GetUserChats===========");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    realm.commitTransaction();
                    realm.close();
                    System.out.println("======GetUserChats===========");
                }

            });
            mSocket.on("newMessageInChat", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    JSONObject data = (JSONObject) args[0];
                    String chatId;
                    try {
                        chatId = data.getString("chatId");
                        JSONObject json = new JSONObject();
                        json.put("id", 123);
                        JSONObject query = new JSONObject();
                        query.put("chatId", chatId);
                        json.putOpt("data", query);
                        mSocket.emit("getChatMessages", json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            });
            mSocket.on("uploadAvatar", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject data = (JSONObject) args[0];
                    try {
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        JSONObject avatar = data.getJSONObject("data");
                        FileRealm avatarRealm;
                        avatarRealm = new FileRealm();
                        avatarRealm.setId(avatar.getString("_id"));
                        avatarRealm.setFileName(avatar.getString("fileName"));
                        avatarRealm.setType(avatar.getString("type"));
                        avatarRealm.setSize(avatar.getInt("size"));
                        avatarRealm = realm.copyToRealmOrUpdate(avatarRealm);
                        JSONObject json = new JSONObject();
                        json.put("id", 123);
                        JSONObject userUpdate = new JSONObject();
                        userUpdate.put("avatar", avatarRealm.getId());
                        json.put("data", userUpdate);
                        mSocket.emit("updateUser", json);
                        realm.commitTransaction();
                        realm.close();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            mSocket.on("updateUser", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject data = (JSONObject) args[0];
                    try {
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        JSONObject user = data.getJSONObject("data");
                        if (!getLocalUserId().equals(user.getString("_id"))) {
                            System.out.println("bad user _id");
                            return;
                        }
                        User userRealm = realm.where(User.class).equalTo("_id", user.getString("_id")).findFirst();
                        if (userRealm == null) {
                            System.out.println("bad user _id");
                            return;
                        }
                        userRealm.setTel(user.getString("tel"));
                        if (user.has("avatar")) {
                            String avatar = user.getString("avatar");
                            FileRealm avatarRealm = realm.where(FileRealm.class).equalTo("id", avatar).findFirst();
                            FileRealm oldAvatar = userRealm.getAvatar();
                            if(oldAvatar != null && !oldAvatar.getId().equals(avatar)){
                                userRealm.setAvatar(null);
                                JSONObject json = new JSONObject();
                                json.put("id",123);
                                JSONObject delFile = new JSONObject();
                                delFile.put("fileId",oldAvatar.getId());
                                delFile.put("isAvatar",true);
                                json.put("data",delFile);
                                mSocket.emit("deleteFile",json);
                                fileService.deleteFile(oldAvatar.getFileName());
                                oldAvatar.deleteFromRealm();
                            }
                            userRealm.setAvatar(avatarRealm);
                            fileService.addDownloadRequest(avatarRealm);
                        }
                        realm.copyToRealmOrUpdate(userRealm);
                        realm.commitTransaction();
                        realm.close();
//256dbea3-aa81-4259-b49c-9f8a62817165
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            mSocket.on("newChat", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    JSONObject data = (JSONObject) args[0];
                    String chatId;
                    try {
                        JSONObject chat = data.getJSONObject("data");
                        Chat chatRealm = realm.where(Chat.class).equalTo("id", chat.getString("_id")).findFirst();
                        if (chatRealm == null) {
                            chatRealm = new Chat();
                            chatRealm.setId(chat.getString("_id"));
                            chatRealm.setChatName(chat.getString("header"));
                            chatRealm.setOwner(chat.getString("owner"));
                        }
                        User member = realm.where(User.class).equalTo("_id", App.this.getLocalUserId()).findFirst();
                        chatRealm.addMember(member);
                        realm.copyToRealmOrUpdate(chatRealm);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    realm.commitTransaction();
                    realm.close();
                }

            });
            mSocket.on("getApkConfig", new Emitter.Listener() {

                @Override
                public void call(Object... args) {

                    JSONObject data = (JSONObject) args[0];
                    String chatId;
                    try {
                        JSONObject apkConfig = data.getJSONObject("data");
                        prefEditor = configSettings.edit();
                        prefEditor.putInt(PREFS_CONFIG_MAX_SIZE_AVATAR, apkConfig.getInt(PREFS_CONFIG_MAX_SIZE_AVATAR));
                        prefEditor.putInt(PREFS_CONFIG_MAX_SIZE_FILE, apkConfig.getInt(PREFS_CONFIG_MAX_SIZE_FILE));
                        prefEditor.putInt(PREFS_CONFIG_POINT_GET_INTERVAL, apkConfig.getInt(PREFS_CONFIG_POINT_GET_INTERVAL));
                        prefEditor.putInt(PREFS_CONFIG_POINT_GET_TIME_INTERVAL, apkConfig.getInt(PREFS_CONFIG_POINT_GET_TIME_INTERVAL));
                        prefEditor.putString(PREFS_CONFIG_STORAGE, apkConfig.getString(PREFS_CONFIG_STORAGE));
                        prefEditor.putString(PREFS_CONFIG_STORAGE_AVATAR_CONTAINER, apkConfig.getString(PREFS_CONFIG_STORAGE_AVATAR_CONTAINER));
                        prefEditor.putString(PREFS_CONFIG_STORAGE_CONTAINER, apkConfig.getString(PREFS_CONFIG_STORAGE_CONTAINER));
                        prefEditor.putString(PREFS_CONFIG_MAX_SIZE_AVATAR, apkConfig.getString(PREFS_CONFIG_MAX_SIZE_AVATAR));
                        prefEditor.apply();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            });
            mSocket.on("userUpdate", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject data = (JSONObject) args[0];
                    try {
                        String userId = data.getString("userId");
                        if (!userId.equals(getLocalUserId())) {
                            JSONObject json = new JSONObject();
                            json.put("id", 123);
                            JSONObject userUpdate = new JSONObject();
                            userUpdate.put("userId", userId);
                            json.put("data", userUpdate);
                            mSocket.emit("getUserById", json);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            mSocket.connect();

        }
    }

    public void createChat(String header) {
        JSONObject json = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            json.put("id", 123);
            data.put("header", header);
            data.put("type", 1);
            json.put("data", data);
            mSocket.emit("newChat", json);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void sendMessage(Message message) {
        JSONObject json = new JSONObject();
        JSONObject data = new JSONObject();
        try {
            json.put("id", 123);
            data.put("chatId", message.getChatId());
            data.put("message", message.getMessage());
            data.put("id",message.getId());
            data.put("type",message.getType());
            if(message.getType()==2){
                JSONObject file = new JSONObject();
                file.put("id",message.getFile().getId());
                file.put("fileName",message.getFile().getFileName());
                file.put("type",message.getFile().getType());
                file.put("size",message.getFile().getSize());
                data.put("file",file);
            }
            json.put("data", data);
            mSocket.emit("newMessage", json);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void uploadFile() {

    }

    public void uploadAvatar(Uri selectedImage) {
        String uuid = UUID.randomUUID().toString();
        File file = new File(selectedImage.toString());
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            String displayName="";
            Cursor cursor = getContentResolver()
                    .query(selectedImage, null, null, null, null, null);

            try {
                // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
                // "if there's anything to look at, look at it" conditionals.
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(
                            cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
            JSONObject json = new JSONObject();
            JSONObject data = new JSONObject();
            try {
                json.put("id", 123);
                data.put("name", displayName);
                data.put("uuid", uuid);
                json.put("data", data);
                mSocket.emit("uploadAvatar",fileInputStream, json);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }



    }

    public Socket getSocket() {
        return mSocket;
    }

    public SharedPreferences getProfileSettings() {
        return profileSettings;
    }

    public SharedPreferences getConfigSettings() {
        return configSettings;
    }

    public String getLocalUserId() {
        return profileSettings.getString(PREF_PROFILE_ID, "");
    }
}
