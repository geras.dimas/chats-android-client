package com.chat.dger.chat.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.chat.dger.chat.App;
import com.chat.dger.chat.R;
import com.chat.dger.chat.view.AddChatView;


import io.realm.Realm;

/**
 * Created by dgerasimov on 14.11.2017.
 */
@InjectViewState
public class AddChatPresenter extends MvpPresenter<AddChatView> {
    public void createChat(String header){

        if(header.length() == 0){
            getViewState().setError(R.id.chat_header_add,"header is empty");
            return;
        }

        App.get().createChat(header);

        getViewState().cancelActivity();
    }
}
