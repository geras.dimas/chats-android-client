package com.chat.dger.chat.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.chat.dger.chat.App;
import com.chat.dger.chat.bean.Chat;
import com.chat.dger.chat.bean.Message;
import com.chat.dger.chat.view.ChatListView;

import java.util.List;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by dgerasimov on 01.11.2017.
 */
@InjectViewState
public class ChatListPresenter extends MvpPresenter<ChatListView> {
    private RealmResults<Chat> realmResults;
    private OrderedRealmCollectionChangeListener<RealmResults<Chat>> changeListener;
    private Realm realm;

    public ChatListPresenter() {
        realm = Realm.getDefaultInstance();
        changeListener = (collection, changeSet) -> {
            // `null`  means the async query returns the first time.
            if (changeSet == null) {
                return;
            }
            // For deletions, the adapter has to be notified in reverse order.
            OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
            for (int i = deletions.length - 1; i >= 0; i--) {
                OrderedCollectionChangeSet.Range range = deletions[i];
            }
            OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
            for (OrderedCollectionChangeSet.Range range : insertions) {
                getChatList();
            }

            OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
            for (OrderedCollectionChangeSet.Range range : modifications) {
                System.out.println("tst");
            }
        };
        realmResults = realm.where(Chat.class).findAll();
        realmResults.addChangeListener(changeListener);
    }

    public void getChatList(){


        getViewState().setChats(realmResults);
    }
}
